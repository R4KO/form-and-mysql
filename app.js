var express = require('express');
var http = require('http');
var bodyParser = require('body-parser');
var helmet = require('helmet');
var rateLimit = require("express-rate-limit");
var mysql = require('mysql');

var app = express();
var server = http.createServer(app);

const limiter = rateLimit({
	windowMs: 15 * 60 * 1000, // 15min
	max: 100 // 100 requests per windowMS for each IP
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(helmet());
app.use(limiter);

var connection = mysql.createConnection({
	host: '',
	user: '',
	password: '',
	database: ''
});

connection.connect(function(err){
	if (err) throw err;

	console.log('J\'dis que j\'suis hors ligne mais j\'suis connecté');
});

connection.end();
// insert
app.post('/add', function(req, res) {
	console.log(req.body);
	res.redirect();
});

server.listen(3000, function(){
	console.log("Server listening on port 3000");
});
